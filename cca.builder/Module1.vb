﻿Imports System.Data.SqlClient
Imports cca.common
Imports cca.objects
Imports System.Net.Mail
Imports System.Net
Imports System.Text
Imports System.Globalization
Imports System.IO


Public Enum dbVersion
    Dev = 0
End Enum

Public Enum SystemGroups As Integer
    AccessToBackendSystems = 1
End Enum

Module Helper

    Dim epwd() As Byte = Nothing

    Dim mdt As DataTable
    Dim mdr As DataRow
    Dim dt As DataTable
    Dim dr As DataRow
    Dim cm As SqlClient.SqlCommand
    Dim CDC As DataCommon
    Dim CO As New SqlCommand()
    Dim tasklist As New Hashtable
    Dim CSI As Integer = 0
    'c:\users\dave.clarke\documents\visual studio 2010\Projects\cca\cca.web\
    Dim webPath As String = "D:\Google Drive\Visual Studio - Websites\cca.web\"
    Dim bldPath As String = "D:\Google Drive\Visual Studio - Projects\cca.objects\cca.objects\"
    'Dim bldPath As String = Environment.ExpandEnvironmentVariables("%USERPROFILE%") & "\Google Drive\Visual Studio - Projects\cca.objects\cca.objects\"
    'Dim pth As String = "C:\Documents and Settings\damocles\bazaar\isatest\dev\task\"
    Dim gnr8 As Boolean = False
    Dim w As Integer = 0

    Sub Main()
        CDC = New DataCommon()
        Dim i As Integer = 0
        For i = 0 To 0
            CDC.CSI = i
            ChangeThings()
            GenerateThings()
            'GenUsers()
            'BuildTaskFlow()
        Next

        If gnr8 Then
            Console.Write("Attempt rebuild?")
            Dim c As Integer = Console.Read()
            If "Yy1".Contains(Chr(c)) Then Rebuild()
        End If

        Console.WriteLine("Done")
        If w = 1 Then Console.ReadLine()
        CDC = Nothing
    End Sub

    Private Sub GenerateThings()
        Select Case CDC.CSI
            Case dbVersion.Dev
                'Generate("agreementfuels", " ")
                'Generate("agreementmeasures", " ")
                'Generate("agreementmeterpoints", " ")
                'Generate("agreements", " ")
                'Generate("agreementtargets", " ")
                'Generate("agreementthroughputs", " ")
                'Generate("agreementtypes", " ")
                'Generate("contactlocations", " ")
                'Generate("contactpurposes", " ")
                'Generate("dataconsumptions", " ")
                'Generate("datathroughputs", " ")
                'Generate("eligibilitythresholds", " ")
                'Generate("emails", " ")
                'Generate("federations", " ")
                'Generate("frequencys", " ")
                'Generate("fuelconversions", " ")
                'Generate("groups", " ")
                'Generate("locations", " ")
                'Generate("locationuses", " ")
                'Generate("messages", " ")
                'Generate("messagetypes", " ")
                'Generate("notifications", " ")
                'Generate("notificationtargets", " ")
                'Generate("persons", " ")
                'Generate("phones", " ")
                'Generate("sessions", " ")
                'Generate("suppliers", " ")
                'Generate("throughputs", " ")
                'Generate("titles", " ")
                'Generate("useremails", " ")
                'Generate("usergroups", " ")
                'Generate("userlocations", " ")
                'Generate("usermessages", " ")
                'Generate("userphones", " ")
                'Generate("users", " ")
                'Generate("systemparameters", " ")
                'GenerateCombo("agreement", "throughput", False)
                'GenerateCombo("user", "location", False)
                'GenerateCombo("user", "phone", False)
                'GenerateCombo("user", "email", False)


                'Generate("matrix", " ")
                'Generate("matrixfiles", " ")
                'Generate("profiles", " ")

                'Generate("matrixtypes", " ")
                'Generate("matrix1details", " ")
                'Generate("matrix2details", " ")
                'Generate("matrix1sources", " ")
                'Generate("matrix2sources", " ")
                'Generate("matrixprices", " ")
                'Generate("ldzs", " ")
                'Generate("ldzpostcodes", " ")
                'Generate("distributions", " ")

                'Generate("bubbles", " ")
                'Generate("bubbleagreements", " ")
                'Generate("bubbletargets", " ")
                'GenerateCombo("bubble", "agreement", False)
                'Generate("sites", " ")
                'Generate("sitereferences", " ")

                'Generate("cclratepercentages", " ")

                'Generate("helpdeskdomains", " ")
                'Generate("helpdesklogondomains", " ")
                'Generate("helpdesklogindetails", " ")

                'Generate("helpdesktemplates", " ")

                'Generate("dataconsumptions", " ")
                'Generate("datathroughputs", " ")
                'Generate("dataimportmethods", " ")

                'Generate("logreports", " ")

                'Generate("cm_rfqs", " ")
                'Generate("cm_rfqsites", " ")


                'Generate("reports", " ")
                'Generate("reporttypes", " ")
                'Generate("reportlogs", "")

                'Generate("agreements", " ")
                'Generate("bills", " ")

                'Generate("helpdesktemplates", " ")
                'Generate("helpdesktemplategroups", " ")
                'Generate("userhelpdesktemplates", " ")
                'GenerateCombo("user", "helpdesktemplate", False)

                'Generate("heldpesk_servicelevels", " ")
                'Generate("helpdeskitem2details", " ")

                'Generate("logreportautomations", " ")
                'Generate("reportautomations", " ")

                'Generate("reportformats", " ")

                'Generate("surveytypes", " ")
                'Generate("surveyurls", " ")
                'Generate("surveys", " ")
                'Generate("surveyanswerfivescales", " ")
                'Generate("survey2details", " ")
                'Generate("survey1details", " ")

                'Generate("surveys", " ")
                'Generate("surveystatuses", " ")


                'Generate("x_survey_requeststagingareas", " ")
                'Generate("helpdeskworkorderdetails", " ")

                'Generate("helpdesklogindetails", " ")

                'Generate("agreementtargets", " ")
                'Generate("agreements", " ")
                'Generate("bubbletargets", " ")

                'Generate("bubbles", " ")


                'Generate("departments", " ")
                'Generate("serviceproducts", " ")
                'Generate("serviceagreements", " ")
                'Generate("organisations", " ")

                'Generate("regions", " ")
                'Generate("sourcinggroups", " ")

                'Generate("notetypes", " ")

                'Generate("notes", " ")

                'Generate("phonecalls", " ")
                'Generate("phonecallnotes", " ")
                'Generate("phonecallpersons", " ")

                'Generate("sitevisitcalls", " ")
                'Generate("sitevisitnotes", " ")
                'Generate("sitevisitpersons", " ")

                'Generate("sitenotes", " ")
                'Generate("organisationnotes", " ")
                'Generate("personnotes", " ")
                'Generate("agreementnotes", " ")


                'Generate("notes", " ")
                'Generate("agreementnotes", " ")
                'Generate("meterpointnotes", " ")
                'Generate("organisationnotes", " ")
                'Generate("personnotes", " ")
                'Generate("phonecallnotes", " ")
                'Generate("sitenotes", " ")
                'Generate("sitevisitnotes", " ")
                'Generate("reportnotes", " ")

                'Generate("agreements", " ")

                'Generate("dataconsumptionanalysis", " ")
                'Generate("agreementrequests", " ")

                'Generate("notetypes", " ")

                'Generate("appointmenttypes", " ")
                'Generate("appointmentstatuses", " ")
                'Generate("appointmentpurposes", " ")
                'Generate("appointments", " ")
                'Generate("appointmentusers", " ")
                'Generate("appointmentnotes", " ")


                'Generate("userstructures", " ")
                'Generate("persons", " ")

                ' Generate("organisationlocations", " ")
                'Generate("surveyanswerfrequencyfivescales", " ")


                'Generate("survey3details", " ")
                'Generate("survey4details", " ")
                'Generate("survey5details", " ")
                'Generate("survey6details", " ")
                'Generate("persons", " ")

                'Generate("survey7details", " ")
                'Generate("survey8details", " ")
                'Generate("reportlogs", "")

                'Generate("surveytimetocompletes", " ")
                'Generate("surveyeaseofcompletes", " ")
                
                'Generate("agreements", " ")
                'Generate("agreementstages", " ")

                'Generate("appointments", " ")

        End Select
    End Sub

    Private Sub ChangeThings()

        Dim CO As New SqlCommand
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure

        Select Case CDC.CSI
            Case dbVersion.Dev

                'CreateNewPerson("Maureen", "Bray", "", "Dunfermline", 0, 0)
                'CreateNewPerson("Laszlo", "Mayer", "", "Budapest", 0, 0)
                'CreateNewPerson("Joanne", "Phillips", "", "Oldham", 0, 0)
                'CreateNewPerson("Mathinee", "Santiwong", "", "Bangkok", 0, 0)

                'CreateNewPerson("Mathinee", "Santiwong", "", "Bangkok", 0, 0)
                'CreateNewPerson("Patchara", "Vutar-uckra", "", "Bangkok", 0, 0)
                'CreateNewPerson("Satta", "Phetju", "", "Bangkok", 0, 0)
                'CreateNewPerson("Videe", "Noppacun", "", "Bangkok", 0, 0)

                'CreateNewPerson("Andrew", "Carter", "", "Oldham", 0, 0)

                'CreateNewPerson("Andras", "Csuman", "", "Budapest", 0, 0)
                'UpdatePassword(32, "hunnybunny")
                'CreateNewPerson("Hugh", "Smith", "", "Oldham", 0, 0)
                'UpdatePassword(168, "cAbbage12")

                'CreateNewPerson("Mateja", "Penava", "", "Budapest", 0, 0)

                'Dim u As New users(1, 1, CDC)
                'u.Read(173)

                'Dim e As New emails(1, 1, CDC)
                'e.email = u.username & "@ems.schneider-electric.com"
                'If e.Save Then
                '    Dim ue As New useremails(1, 1, CDC)
                '    ue.user_fk = u.user_pk
                '    ue.email_fk = e.email_pk
                '    ue.contactlocation_fk = 2
                '    If ue.Save Then
                '        MsgBox("Saved")
                '    End If
                'End If

                'CreateNewPerson("Arun", "Singh", "", "Bangkok", 0, 0)
                'CreateNewPerson("Pongpiya", "Chaimar", "", "Bangkok", 0, 0)
                'CreateNewPerson("Chutima", "Thongnor", "", "Bangkok", 0, 0)
                'CreateNewPerson("Natsinee", "Pong", "", "Bangkok", 0, 0)


                'CreateNewPerson("Mila", "Krivokapic", "", "Budapest", 0, 0)
                'CreateNewPerson("Krisztina", "Jonas", "", "Budapest", 0, 0)
                'CreateNewPerson("Zsofia", "Szakal", "", "Budapest", 0, 0)

                'Dim sa As New surveyanswerfrequencyfivescales(1, 1, CDC) : sa.surveyanswername = "Almost Never" : sa.Save()
                'sa = New surveyanswerfrequencyfivescales(1, 1, CDC) : sa.surveyanswername = "Rarely" : sa.Save()
                'sa = New surveyanswerfrequencyfivescales(1, 1, CDC) : sa.surveyanswername = "Frequently" : sa.Save()
                'sa = New surveyanswerfrequencyfivescales(1, 1, CDC) : sa.surveyanswername = "Almost Always" : sa.Save()
                'sa = New surveyanswerfrequencyfivescales(1, 1, CDC) : sa.surveyanswername = "N/A" : sa.Save()


                'CreateNewPerson("Katalin", "Megyeri", "", "Budapest", 0, 0)


                'UpdatePassword(168, "cAbbage12")


                'Dim st As New surveytypes(1, 1, CDC) 
                'st = New surveytypes(1, 1, CDC) : st.surveytypename = "German Tender Requests - Requester" : st.Save()
                'st = New surveytypes(1, 1, CDC) : st.surveytypename = "German Tender Requests - Technician" : st.Save()

                'CreateNewPerson("Nuttapon", "Komsai", "", "Bangkok", 0, 0)
                'UpdatePassword(167, "kisnyuszi")

                'Dim x As New appointmenttypes(1, 1, CDC)

                'x.Read(3) : x.Disable()
                'x.Read(4) : x.Disable()
                'UpdatePassword(180, "letmein123")

                'CreateNewPerson("ichaya", "koodeedang", "", "Bangkok", 0, 0)
                'CreateNewPerson("wasamon", "lamluang", "", "Bangkok", 0, 0)

                'CreateNewPerson("Adam", "Kasnyik", "", "Budapest", 0, 0)
                'CreateNewPerson("Georgina", "Gorog", "", "Budapest", 0, 0)
                'CreateNewPerson("Katalin", "Sepa", "", "Budapest", 0, 0)
                'CreateNewPerson("Viktoria", "Varga", "", "Budapest", 0, 0)

                'CreateNewPerson("Adrienn", "Pasztoy", "", "Budapest", 0, 0)
                'CreateNewPerson("Andras", "Pulay", "", "Budapest", 0, 0)
                'CreateNewPerson("Anna", "Sohar", "", "Budapest", 0, 0)
                'CreateNewPerson("Bozo", "Pintaric", "", "Budapest", 0, 0)
                'CreateNewPerson("Kinga", "Kovacs", "", "Budapest", 0, 0)
                'CreateNewPerson("Kristof", "Szegedi", "", "Budapest", 0, 0)
                'CreateNewPerson("Nora", "Finszter", "", "Budapest", 0, 0)

                'CreateNewPerson("Alexander", "Inglis", "", "Budapest", 0, 0)
                'CreateNewPerson("Anar", "Taghiyev", "", "Budapest", 0, 0)
                'CreateNewPerson("Arzu", "Hajiyeva", "", "Budapest", 0, 0)
                'CreateNewPerson("Balazs", "Olah", "", "Budapest", 0, 0)
                'CreateNewPerson("Balint", "Balazs", "", "Budapest", 0, 0)
                'CreateNewPerson("Bence", "Fadgyas", "", "Budapest", 0, 0)
                'CreateNewPerson("Benjamin", "Feldmajer", "", "Budapest", 0, 0)
                'CreateNewPerson("David", "Ali", "", "Budapest", 0, 0)
                'CreateNewPerson("Dmitriy", "Pigildin", "", "Budapest", 0, 0)
                'CreateNewPerson("Dora", "Bay", "", "Budapest", 0, 0)
                'CreateNewPerson("Edina", "Csongor", "", "Budapest", 0, 0)
                'CreateNewPerson("Eduardo", "Cortez", "", "Budapest", 0, 0)
                'CreateNewPerson("Erik", "Galbats", "", "Budapest", 0, 0)
                'CreateNewPerson("Eva", "Metcalf", "", "Budapest", 0, 0)
                'CreateNewPerson("Gabriella", "Somfai", "", "Budapest", 0, 0)
                'CreateNewPerson("Hajnalka", "Gaal", "", "Budapest", 0, 0)
                'CreateNewPerson("Ina", "Mancellari", "", "Budapest", 0, 0)
                'CreateNewPerson("Iryna", "Migel", "", "Budapest", 0, 0)
                'CreateNewPerson("Jacint", "Polgar", "", "Budapest", 0, 0)
                'CreateNewPerson("Krisztina", "Bekker", "", "Budapest", 0, 0)
                'CreateNewPerson("Lejla", "Mesic", "", "Budapest", 0, 0)
                'CreateNewPerson("Mark", "Jelencsik", "", "Budapest", 0, 0)
                'CreateNewPerson("Mihaly", "Bor", "", "Budapest", 0, 0)
                'CreateNewPerson("Orsolya", "Csige", "", "Budapest", 0, 0)
                'CreateNewPerson("Peter", "Heredi", "", "Budapest", 0, 0)
                'CreateNewPerson("Ruari", "Cairns", "", "Budapest", 0, 0)
                'CreateNewPerson("Tibor", "Totarvaj", "", "Budapest", 0, 0)
                'CreateNewPerson("Viktor", "Peterdi", "", "Budapest", 0, 0)
                'CreateNewPerson("Wayne", "Metcalf", "", "Budapest", 0, 0)



                'CreateNewPerson("Kunthira", "Disayabuth", "", "Bangkok", 0, 0)
                'CreateNewPerson("Agnes", "Sandor", "", "Budapest", 0, 0)

                'Dim x As New surveytimetocompletes(1, 1, CDC)

                'x = New surveytimetocompletes(1, 1, CDC) : x.surveytimename = "Less than 5 minutes" : x.Save()
                'x = New surveytimetocompletes(1, 1, CDC) : x.surveytimename = "5 - 15 minutes" : x.Save()
                'x = New surveytimetocompletes(1, 1, CDC) : x.surveytimename = "15 - 25 minutes" : x.Save()
                'x = New surveytimetocompletes(1, 1, CDC) : x.surveytimename = "More than 25 minutes" : x.Save()

                'Dim y As New surveyeaseofcompletes(1, 1, CDC)

                'y = New surveyeaseofcompletes(1, 1, CDC) : y.surveyeasename = "Very Easy" : y.Save()
                'y = New surveyeaseofcompletes(1, 1, CDC) : y.surveyeasename = "Easy" : y.Save()
                'y = New surveyeaseofcompletes(1, 1, CDC) : y.surveyeasename = "Difficult" : y.Save()
                'y = New surveyeaseofcompletes(1, 1, CDC) : y.surveyeasename = "Very Difficult" : y.Save()

                'CreateNewPerson("Erika", "Hornok", "", "Budapest", 0, 0)

                'SendSupplierSurvey(235, 3)
                'SendSupplierSurvey(235, 4)
                'SendSupplierSurvey(235, 5)
                'SendSupplierSurvey(235, 6)

                'CreateNewPerson("Fanni", "Cifka", "", "Budapest", 0, 0)
                'CreateNewPerson("Zoltan", "Varadi", "", "Budapest", 0, 0)

                'SendSupplierSurvey(82, 3)
                'SendSupplierSurvey(83, 3)
                'SendSupplierSurvey(112, 3)
                'SendSupplierSurvey(139, 3)
                'SendSupplierSurvey(135, 3)
                'SendSupplierSurvey(99, 3)
                'SendSupplierSurvey(170, 3)
                'SendSupplierSurvey(90, 3)
                'SendSupplierSurvey(169, 4)
                'SendSupplierSurvey(128, 4)
                'SendSupplierSurvey(192, 4)
                'SendSupplierSurvey(194, 4)
                'SendSupplierSurvey(193, 4)
                'SendSupplierSurvey(195, 6)
                'SendSupplierSurvey(196, 6)
                'SendSupplierSurvey(197, 6)
                'SendSupplierSurvey(198, 6)
                'SendSupplierSurvey(236, 6)
                'SendSupplierSurvey(237, 6)
                'SendSupplierSurvey(201, 6)
                'SendSupplierSurvey(209, 5)
                'SendSupplierSurvey(38, 5)
                'SendSupplierSurvey(206, 5)
                'SendSupplierSurvey(211, 5)
                'SendSupplierSurvey(224, 5)
                'SendSupplierSurvey(213, 5)
                'SendSupplierSurvey(212, 5)
                'SendSupplierSurvey(126, 5)
                'SendSupplierSurvey(207, 5)
                'SendSupplierSurvey(217, 5)
                'SendSupplierSurvey(214, 5)

                'UpdatePassword(34, "C4bbage5")

                'Dim x As New helpdesklogindetails(1, 1, CDC)
                'x.helpdeskdomain_fk = 1
                'x.helpdesklogondomain_fk = 1
                'x.helpdeskuserid = 32157
                'x.password = "asdfuh7823sd"
                'x.username = "mila.krivokapic"
                'x.user_fk = 178
                'x.Save()


                'CreateNewPerson("Sailada", "Witooteerasan", "", "Bangkok", 0, 0)

                'SendSupplierSurvey(97, 3)
                'SendSupplierSurvey(108, 3)

                'Dim x As New helpdesklogindetails(1, 1, 150, CDC)
                'x.helpdeskuserid = 31501
                'x.Save()

                'Dim x As New agreementstages(1, 1, CDC)
                'x = New agreementstages(1, 1, CDC) : x.agreementstagename = "Review" : x.Save()
                'x = New agreementstages(1, 1, CDC) : x.agreementstagename = "Acquisition" : x.Save()
                'x = New agreementstages(1, 1, CDC) : x.agreementstagename = "Management" : x.Save()

                'CreateNewPerson("Fanni", "Denke", "", "Budapest", 0, 0)

                'UpdatePassword(29, "letmein")

                'CreateNewPerson("Rapeeard", "Chaiwatanasaransuk", "rapeeard.chai", "Bangkok", 0, 0)

                'CreateNewPerson("Jorge", "Enciso", "", "Budapest", 0, 0)

                'CreateNewPerson("Beata", "Turanyi", "", "Budapest", 0, 0)
                'CreateNewPerson("Beata", "Fritz", "", "Budapest", 0, 0)
                'CreateNewPerson("Ildiko", "Csanya", "", "Budapest", 0, 0)

                'CreateNewPerson("Hector", "Fernandez", "", "Oldham", 0, 0)
                'CreateNewPerson("Thomas", "Bardwell", "tom.bardwell", "Oldham", 0, 0, "WelcomeCCA")

                'CreateNewPerson("Tamas", "Szabo", "", "Budapest", 0, 0)

                'CreateNewPerson("Richard", "Hurford", "", "Oldham", 0, 0, "WelcomeDemand")
                'CreateNewPerson("Paul", "Wrighton", "", "Oldham", 0, 0, "WelcomeDemand")
                'UpdatePassword(165, "WelcomeDemand")

                'CreateNewPerson("Matthew", "Hurrell", "", "Oldham", 0, 0, "WelcomeCCA")
                'CreateNewPerson("Adrienn", "Nemeth", "", "Budapest", 0, 0, "WelcomeCCA")

                'CreateNewPerson("Arun", "Singh", "", "Bangkok", 0, 0, "WelcomeCM")

                'CreateNewPerson("Martina", "Mehes", "", "Budapest", 0, 0, "WelcomeCCA")
                'CreateNewPerson("Daniel", "Raport", "", "Budapest", 0, 0, "WelcomeCCA")

                'CreateNewPerson("Borbala", "Szabo", "", "Budapest", 0, 0, "WelcomeCCA")

                'CreateNewPerson("Katalin", "Szirmay", "", "Budapest", 0, 0, "WelcomeCM")
                'CreateNewPerson("Gabor", "Szabo", "", "Budapest", 0, 0, "WelcomeCM")

                'CreateNewPerson("Sara", "Honti", "", "Budapest", 0, 0, "WelcomeCM")
                'CreateNewPerson("Eszter", "Bansaghi", "", "Budapest", 0, 0, "WelcomeCM")

                'CreateNewPerson("Dorottya", "Olah", "", "Budapest", 0, 0, "WelcomeCCA")

                'Dim r As New reports(1, 1, CDC)
                'r.reportname = "MPMA Bubble Report"
                'r.reporttype_fk = 2
                'r.reportserverurl = "http://reports.mceg.local/ReportServer12/"
                'r.reportpath = "/UML/One View/MTS Reports/CCLA MTS - Bubble Report MPMA v2.0"
                'r.Save()


                'CreateNewPerson("Saffi", "Taher", "", "Oldham", 0, 0, "WelcomeCM")
                'CreateNewPerson("Warakorn", "Chusin", "", "Bangkok", 0, 0, "WelcomeBKK")

                'CreateNewPerson("Saada", "Amadu", "", "Budapest", 0, 0, "WelcomeCCA")


                'Dim r As New reports(1, 1, CDC)
                'r.reportname = "Bubble Level Data Export"
                'r.reporttype_fk = 7
                'r.reportserverurl = "http://reports.mceg.local/ReportServer12/"
                'r.reportpath = "/UML/One View/CCA Reports/One View Data Export - Bubble"
                'r.Save()

                'CreateNewPerson("Zsofia", "Kegl", "", "Budapest", 0, 0, "WelcomeBP")
                'CreateNewPerson("Nenand", "Obradovic", "", "Budapest", 0, 0, "WelcomeCCA")

                'UpdatePassword(29, "WelcomeIain")
                'CreateNewPerson("Krisztina", "Verbics", "", "Budapest", 0, 0, "WelcomeCCA")

                'UpdatePassword(186, "WelcomeBKK")
                'CreateNewPerson("Supakorn", "B", "", "Bangkok", 0, 0, "WelcomeBKK")

                'CreateNewPerson("Richard", "Jozsa", "", "Budapest", 0, 0, "WelcomeCCA")
                'CreateNewPerson("Renato", "Vilsitz", "", "Budapest", 0, 0, "WelcomeCCA")

                'alison.maitland@ems.schneider-electric.com
                'waleed.chaudhry@ems.schneider-electric.com
                'konstantinos.anastasiou@ems.schneider-electric.com

                'CreateNewPerson("Alison", "Maitland", "", "Oldham", 0, 0, "WelcomeOV")
                'CreateNewPerson("Waleed", "Chaudhry", "", "Budapest", 0, 0, "WelcomeOV")
                'CreateNewPerson("Konstantinos", "Anastasiou", "", "Budapest", 0, 0, "WelcomeOV")

                'CreateNewPerson("Carol", "Nicholson", "", "Dunfermline", 0, 0, "WelcomeCM")

                'UpdatePassword(160, "mydogiscalledbrian")

                'Dim r As New reports(1, 1, CDC)
                'r.reportname = "One View Agreement Audit Report"
                'r.reporttype_fk = 7
                'r.reportserverurl = "http://reports.mceg.local/ReportServer/"
                'r.reportpath = "/UML/One View/CCA Reports/OneView Agreement Audit Report"
                'r.Save()


                'CreateNewPerson("Zita", "Nyamadi", "", "Budapest", 0, 0, "WelcomeCCA")
                'CreateNewPerson("Joseph", "McClelland", "joe.mcclelland", "Oldham", 0, 0, "WelcomeDemand")

                'UpdatePassword(316, "NewcastleUtd")
                'CreateNewPerson("Kenny", "McCartney", "", "Oldham", 0, 0, "CelticUtd")


        End Select

    End Sub

    Private Sub SendSupplierSurvey(ByVal user_fk As Integer, ByVal surveytype_fk As Integer)
        If surveytype_fk >= 3 And surveytype_fk <= 6 Then
            Dim user As Integer = user_fk '1 ' 82
            Dim surveytype As Integer = surveytype_fk '3

            Dim strFile As String = "c:\logs\supplier surveys\"
            strFile = Replace(strFile, "bin\Debug\", "")
            If Not Directory.Exists(strFile) Then
                Directory.CreateDirectory(strFile)
            End If
            strFile &= "log_" & DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss") & ".txt"

            Dim fs As FileStream = Nothing
            If (Not File.Exists(strFile)) Then
                Try
                    fs = File.Create(strFile)
                    fs.Close()
                Catch ex As Exception

                End Try
            End If

            Dim sw As StreamWriter = File.AppendText(strFile)
            Log("Started", sw)


            Dim url As String = ""
            Dim email As String = ""
            Dim name As String = ""
            Dim body As String = ""
            Dim subject As String = ""
            Dim sd As Object

            If surveytype = 3 Then
                sd = New survey3details(35, 35, CDC)
            ElseIf surveytype = 4 Then
                sd = New survey4details(35, 35, CDC)
            ElseIf surveytype = 5 Then
                sd = New survey5details(35, 35, CDC)
            ElseIf surveytype = 6 Then
                sd = New survey6details(35, 35, CDC)
            End If

            Dim su As New surveyurls(35, 35, CDC)

            Dim s As New surveys(35, 35, CDC)
            s.surveytype_fk = surveytype
            s.surveydetail_fk = -1
            s.surveystatus_fk = 1

            If s.Save Then

                If surveytype = 3 Then
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 95 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 57 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 39 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 4 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 40 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 22 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 31 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey3details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 65 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()

                ElseIf surveytype = 4 Then
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 95 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 57 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 39 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 4 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 40 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 22 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 31 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey4details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 65 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()

                ElseIf surveytype = 5 Then
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 95 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 57 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 39 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 4 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 40 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 22 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 31 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey5details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 65 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()

                ElseIf surveytype = 6 Then
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 95 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 57 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 39 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 4 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 40 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 22 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 31 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()
                    sd = New survey6details(35, 35, CDC) : sd.selecteduser_fk = user : sd.supplier_fk = 65 : sd.survey_fk = s.survey_pk : sd.summary3_otherinformation = "" : sd.Save()

                End If

                CO = New SqlCommand
                CO.CommandType = CommandType.StoredProcedure
                CO.CommandText = "rsp_surveyurls_generate"
                CDC.ReadScalarValue(url, CO)

                su = New surveyurls(35, 35, CDC)
                su.survey_fk = s.survey_pk
                su.url = url

                If su.Save Then

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CO.CommandText = "SELECT [se_cca].[dbo].[primaryemailoffice](" & user & ")"
                    CDC.ReadScalarValue(email, CO)

                    CO = New SqlCommand
                    CO.CommandType = CommandType.Text
                    CO.CommandText = "SELECT [se_cca].[dbo].[formatpersonname](" & user & ",3)"
                    CDC.ReadScalarValue(name, CO)

                    Dim st As New surveytypes(35, 35, surveytype, CDC)
                    subject = st.surveytypename & " - " & DateTime.Now.ToString("dd MMM yyyy")

                    body = "Hello " & name & ", <br />"
                    body &= "<br />"
                    body &= "Please could you take a couple of minutes to give your feedback on the performance of the energy suppliers listed in the survey.<br /><br />"
                    body &= "<b>URL:</b> <a href='dev.utilitymasters.co.uk/coresurvey/suppliersurvey.aspx?surveyurl=" & url & "'>LINK</a>"
                    SendSMTP("donotreply@ems.schneider-electric.com", RemoveDiacritics(email), subject, body, "", "HTML")
                    Log(Now & " | Supplier Survey (" & s.survey_pk & ") sent to " & email, sw)
                End If


            End If



        End If

    End Sub

    Public Sub Log(logMessage As String, w As TextWriter)
        Console.WriteLine(logMessage)
        w.Write(vbCrLf)
        w.Write("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString())
        w.Write(" : {0}", logMessage)
    End Sub

    Private Function RemoveDiacritics(text As String) As String
        Dim normalizedString = text.Normalize(NormalizationForm.FormD)
        Dim stringBuilder = New StringBuilder()

        For Each c As Char In normalizedString
            Dim unicodeCategory__1 = CharUnicodeInfo.GetUnicodeCategory(c)
            If unicodeCategory__1 <> UnicodeCategory.NonSpacingMark Then
                stringBuilder.Append(c)
            End If
        Next

        Return stringBuilder.ToString().Normalize(NormalizationForm.FormC)
    End Function

    Private Function generatepassword() As String
        Dim password As String = ""

        CO.CommandText = "rsp_CreatePasswd"
        CDC.ReadScalarValue(password, CO)
        CO.Parameters.Clear()

        Return password
    End Function

    Private Sub CreateNewPerson(ByVal forename As String, ByVal surname As String, ByVal usrname As String, ByVal office As String, Optional ByVal readlevel As Integer = 1, Optional ByVal writelevel As Integer = 1, Optional ByVal password As String = "")
        Dim username As String = Nothing
        Dim email As String = ""
        Dim useremail As String = ""


        Dim CO As New SqlCommand
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure

        CO.CommandText = "rsp_CreateUserName"
        CO.Parameters.AddWithValue("@fn", forename.ToLower)
        CO.Parameters.AddWithValue("@sn", surname.ToLower)
        CDC.ReadScalarValue(username, CO)
        CO.Parameters.Clear()

        If usrname.Length > 0 Then : username = usrname : End If

        If password.Length < 5 Then
            CO.CommandText = "rsp_CreatePasswd"
            CDC.ReadScalarValue(password, CO)
            CO.Parameters.Clear()
        End If

        CO.CommandText = "rsp_CreatePasswdEnc"
        CO.Parameters.AddWithValue("@pwd", password)
        CDC.ReadScalarValue(epwd, CO)
        CO.Parameters.Clear()

        Dim u As New users(-1, -1, CDC)
        u.username = username
        u.encpassword = epwd
        u.readlevel = readlevel
        u.writelevel = writelevel
        If u.Save() Then
            Dim p As New persons(u.user_pk, u.user_pk, CDC)
            p.forename = forename
            p.surname = surname
            p.user_fk = u.user_pk
            p.middlename = ""
            p.jobtitle = ""
            If p.Save() Then
                Dim e As New emails(u.user_pk, u.user_pk, CDC)
                'e.email = Replace(forename.ToLower & "." & surname.ToLower, " ", ".") & "@non.se.com"
                e.email = Replace(forename.ToLower & "." & surname.ToLower, " ", ".") & "@ems.schneider-electric.com"
                email = e.email
                If e.Save() Then
                    Dim ue As New useremails(u.user_pk, u.user_pk, CDC)
                    ue.user_fk = u.user_pk
                    ue.email_fk = e.email_pk
                    ue.contactlocation_fk = 2
                    If ue.Save() Then
                        useremail = ue.useremail_pk
                        Dim l As New locations(u.user_pk, u.user_pk, CDC)
                        Select Case office
                            Case "Oldham" : l.Read(1)
                            Case "Dunfermline" : l.Read(2)
                            Case "Budapest" : l.Read(3)
                            Case "Bangkok" : l.Read(4)
                        End Select
                        Dim ul As New userlocations(u.user_pk, u.user_pk, CDC)
                        ul.user_fk = u.user_pk
                        ul.location_fk = l.location_pk
                        ul.locationuse_fk = 1
                        If ul.Save() Then
                            Dim ph As New phones(u.user_pk, u.user_pk, CDC)

                            Select Case office
                                Case "Oldham" : ph.Read(2)
                                Case "Dunfermline" : ph.Read(3)
                                Case "Budapest" : ph.Read(4)
                                Case "Bangkok" : ph.Read(6)
                            End Select
                            Dim uph As New userphones(u.user_pk, u.user_pk, CDC)
                            uph.user_fk = u.user_pk
                            uph.phone_fk = ph.phone_pk
                            uph.contactlocation_fk = 2
                            uph.Save()
                        End If
                    End If
                End If
            End If

            Dim ug As New usergroups(u.user_pk, u.user_pk, CDC) ' add to the staff group
            ug.user_fk = u.user_pk
            ug.group_fk = 4
            ug.Save()

        End If

        Dim strBody As String = ""

        strBody &= "UserID: " & u.user_pk & "<br />"
        strBody &= "Username: " & u.username & "<br />"
        strBody &= "Password: " & password & "<br />"
        strBody &= "Email: " & email & "<br />"
        strBody &= "useremail_pk: " & useremail & "<br />"
        strBody &= "URL: <a href='dev.utilitymasters.co.uk'>dev.utilitymasters.co.uk</a>"


        SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "New User Created", strBody, "", "HTML")
    End Sub

    Private Sub UpdatePassword(ByVal user_fk As Integer, ByVal password As String)

        Dim CO As New SqlCommand
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure

        CO.CommandText = "rsp_CreatePasswdEnc"
        CO.Parameters.AddWithValue("@pwd", password)
        CDC.ReadScalarValue(epwd, CO)
        CO.Parameters.Clear()

        Dim u As New users(-1, -1, user_fk, CDC)
        u.encpassword = epwd
        u.Save()

        Dim strBody As String = ""

        strBody &= "UserID: " & u.user_pk & "<br />"
        strBody &= "Username: " & u.username & "<br />"
        strBody &= "Password: " & password & "<br />"
        strBody &= "URL: <a href='dev.utilitymasters.co.uk'>dev.utilitymasters.co.uk</a>"


        SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Password Updated", strBody, "", "HTML")


    End Sub

    Private Sub SendSMTP(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String, Optional ByVal format As String = "Text", Optional ByVal BCC As String = "", Optional ByVal User As String = "", Optional ByVal Pass As String = "")
        Dim htmlMessageBody As String
        htmlMessageBody = " "
        htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
        htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
        htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
        htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " </STYLE>"
        htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
        htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
        htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
        htmlMessageBody = htmlMessageBody + " <P>"
        htmlMessageBody = htmlMessageBody + strBody
        htmlMessageBody = htmlMessageBody + "</P>"
        htmlMessageBody = htmlMessageBody + " </TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
        htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"



        Dim insMail As New MailMessage(New MailAddress(strFrom), New MailAddress(strTo))
        With insMail
            .Subject = strSubject
            If format = "HTML" Then : .IsBodyHtml = True : Else : .IsBodyHtml = True : End If
            .Body = htmlMessageBody
            If BCC.Length > 0 Then
                .Bcc.Add(New MailAddress(BCC))
            End If
            .Bcc.Add(New MailAddress("dave.clarke@ems.schneider-electric.com"))
            If Not strAttachments.Equals(String.Empty) Then
                Dim strFile As String
                Dim strAttach() As String = strAttachments.Split(";"c)
                For Each strFile In strAttach
                    .Attachments.Add(New System.Net.Mail.Attachment(strFile.Trim()))
                Next
            End If
        End With
        Dim smtp As New System.Net.Mail.SmtpClient

        If User.Length > 0 Then
            smtp.Credentials = New NetworkCredential(User, Pass)
            'mySmtpClient.Credentials = New NetworkCredential("MCEG\terminations", "Password99")
            'mySmtpClient.Credentials = New Net.NetworkCredential("administrator@ads.utilitymasters.co.uk", "chicago")
        Else
            smtp.Credentials = CredentialCache.DefaultNetworkCredentials
        End If

        smtp.Host = "10.44.33.23"
        smtp.Port = 25
        smtp.Send(insMail)
    End Sub

    Private Function SatNav(ByVal sourcepcode As String, ByVal targetpcode As String) As DataSet
        Dim _Return As New DataSet

        If Not sourcepcode Is Nothing Then

            Dim strURL As String = Nothing
            strURL = "http://maps.googleapis.com/maps/api/distancematrix/xml?origins=" & sourcepcode & "&destinations=" & targetpcode & "&mode=driving&language=en-GB&units=imperial&sensor=false"

            _Return.ReadXml(strURL)

        End If

        Return _Return
    End Function

    Private Sub Rebuild()

        IO.File.Copy(bldPath & "\cca.Objects.vbproj", bldPath & "cca.Objects.vbproj.bak", True)
        Dim d As New System.Xml.XmlDocument
        d.Load(bldPath & "\cca.Objects.vbproj")
        Dim r As System.Xml.XmlElement = d.DocumentElement
        Dim nGrp As System.Xml.XmlNode
        Dim nFl As System.Xml.XmlNode
        Dim nL As System.Xml.XmlNodeList
        Dim x As Integer
        Dim addHere As Boolean = False
        For Each nGrp In r.ChildNodes
            If nGrp.LocalName = "ItemGroup" Then
                nL = nGrp.ChildNodes
                For x = (nL.Count - 1) To 0 Step -1
                    nFl = nL(x)
                    If nFl.LocalName = "Compile" And nFl.HasChildNodes = False Then
                        nGrp.RemoveChild(nFl)
                        addHere = True
                    End If
                Next
                If addHere Then
                    Dim fls() As String = IO.Directory.GetFiles(bldPath, "*.vb")
                    Dim fl As String = ""
                    Dim nE As System.Xml.XmlElement
                    Dim nA As System.Xml.XmlAttribute
                    For Each fl In fls
                        nE = d.CreateElement("Compile")
                        nA = d.CreateAttribute("Include")
                        nA.Value = fl.Replace(bldPath & "\", "")
                        nE.Attributes.Append(nA)
                        nGrp.AppendChild(nE)
                    Next
                    addHere = False
                End If
            End If
        Next

        d.Save(bldPath & "\cca.Objects.vbproj")

        IO.File.WriteAllText(bldPath & "\cca.Objects.vbproj", IO.File.ReadAllText(bldPath & "\cca.Objects.vbproj").Replace(" xmlns=""""", ""))

        d = Nothing
        Environment.CurrentDirectory = bldPath
        Dim p As ProcessStartInfo
        p = New ProcessStartInfo
        p.WorkingDirectory = bldPath
        p.Arguments = "cca.Objects.sln /t:rebuild /p:Configuration=Release"
        p.FileName = "C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\msbuild.exe"

        Process.Start(p)

    End Sub

    Private Sub Generate(ByVal tblName As String, Optional ByVal tblExclude As String = " ")
        Dim exclude As String
        gnr8 = True
        Select Case tblExclude
            Case ""
                exclude = ""
            Case " "
                exclude = "not (TABLE_NAME like '%logs') and TABLE_NAME in (select TABLE_NAME from [rkg_tables]) and"
            Case Else
                exclude = "not (TABLE_NAME like '" & tblExclude & "') and"
        End Select

        Dim MO As New SqlClient.SqlCommand("select distinct TABLE_NAME from [rkg_columns] where " & exclude & " table_name like '" & tblName & "'")
        mdt = CDC.ReadDataTable(MO)
        Dim CO As SqlClient.SqlCommand
        Dim fl As IO.StreamWriter
        'Dim sqlfl As IO.StreamWriter
        Dim wwwfl As IO.StreamWriter
        Dim wcbfl As IO.StreamWriter
        For Each mdr In mdt.Rows
            Console.WriteLine(mdr("TABLE_NAME"))
            fl = New IO.StreamWriter(bldPath & "\" & mdr("TABLE_NAME") & ".vb", False)
            CO = New SqlClient.SqlCommand("select * from [rkg_columns] where TABLE_NAME like '" & mdr("TABLE_NAME") & "'")
            dt = CDC.ReadDataTable(CO)
            Dim cPG As New PropertyGenerator(dt)
            fl.WriteLine("Imports cca.common")
            fl.WriteLine("Public Class " & mdr("TABLE_NAME"))
            fl.WriteLine(cPG.Properties)
            Dim cFG As New FunctionGenerator(dt)
            fl.WriteLine(cFG.Functions)
            fl.WriteLine("End Class")
            fl.Flush()
            fl.Close()
            'IO.File.Copy(bldPath & mdr("TABLE_NAME") & ".vb", webPath & "app_code\" & mdr("TABLE_NAME") & ".vb", True)

            Dim cSG As New SqlGenerator(dt, CDC, CDC.CSI)
            'sqlfl = New IO.StreamWriter("c:\grem\output\sql\" & mdr("TABLE_NAME") & ".sql", False)
            'sqlfl.WriteLine(cSG.Sql)
            'sqlfl.Flush()
            'sqlfl.Close()

            wwwfl = New IO.StreamWriter(webPath & "controls\" & mdr("TABLE_NAME") & "_ctl.ascx", False)
            wcbfl = New IO.StreamWriter(webPath & "controls\" & mdr("TABLE_NAME") & "_ctl.ascx.vb", False)
            Dim cWC As New WebControlGenerator(dt)
            wwwfl.WriteLine(cWC.WebControl)
            wwwfl.Flush()
            wwwfl.Close()

            wcbfl.WriteLine(cWC.CodeBehind)
            wcbfl.Flush()
            wcbfl.Close()

            Console.WriteLine("Wrote " & mdr("TABLE_NAME"))
        Next

    End Sub

    Private Sub GenerateCombo(ByVal MasterTable As String, ByVal SubTable As String, ByVal AddManage As Boolean)
        Dim cCC As New WebCtlComboGen(MasterTable, SubTable, AddManage, CDC)

        Dim fl1, fl2, fl3, fl4, fl5 As IO.StreamWriter

        If MasterTable = "user" Then
            fl1 = New IO.StreamWriter(webPath & "controls\combo\person" & SubTable & "s.ascx", False)
            fl2 = New IO.StreamWriter(webPath & "controls\combo\person" & SubTable & "s.ascx.vb", False)
            fl3 = New IO.StreamWriter(webPath & "controls\combo\user" & SubTable & "_combo_ctl.ascx", False)
            fl4 = New IO.StreamWriter(webPath & "controls\combo\user" & SubTable & "_combo_ctl.ascx.vb", False)
            fl5 = New IO.StreamWriter(webPath & "functions\remove" & SubTable & ".ashx", False)

            fl1.Write(cCC.PersonsWebControl) : fl1.Flush() : fl1.Close()
            fl2.Write(cCC.PersonsCodeBehind) : fl2.Flush() : fl2.Close()
            fl3.Write(cCC.WebControl) : fl3.Flush() : fl3.Close()
            fl4.Write(cCC.CodeBehind) : fl4.Flush() : fl4.Close()
            fl5.Write(cCC.PersonsFunction) : fl5.Flush() : fl5.Close()
        Else
            fl1 = New IO.StreamWriter(webPath & "controls\combo\" & MasterTable & "" & SubTable & "s.ascx", False)
            fl2 = New IO.StreamWriter(webPath & "controls\combo\" & MasterTable & "" & SubTable & "s.ascx.vb", False)
            fl3 = New IO.StreamWriter(webPath & "controls\combo\" & MasterTable & "" & SubTable & "_combo_ctl.ascx", False)
            fl4 = New IO.StreamWriter(webPath & "controls\combo\" & MasterTable & "" & SubTable & "_combo_ctl.ascx.vb", False)
            fl5 = New IO.StreamWriter(webPath & "functions\remove" & SubTable & ".ashx", False)

            fl1.Write(cCC.PersonsWebControl) : fl1.Flush() : fl1.Close()
            fl2.Write(cCC.PersonsCodeBehind) : fl2.Flush() : fl2.Close()
            fl3.Write(cCC.WebControl) : fl3.Flush() : fl3.Close()
            fl4.Write(cCC.CodeBehind) : fl4.Flush() : fl4.Close()
            fl5.Write(cCC.PersonsFunction) : fl5.Flush() : fl5.Close()

        End If

        Console.WriteLine("Wrote {0}-{1}", MasterTable, SubTable)

    End Sub

End Module